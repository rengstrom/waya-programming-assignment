package com.wayatask.Fruits.framework.repository;

import com.wayatask.Fruits.domain.model.FruitBasket;
import com.wayatask.Fruits.domain.model.FruitVendor;
import com.wayatask.Fruits.domain.repository.FruitVendorRepository;
import com.wayatask.Fruits.framework.model.InputData;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

public class FruitVendorInMemoryRepository implements FruitVendorRepository {

    Map<Integer, FruitVendor> fruitVendors;

    public FruitVendorInMemoryRepository(InputData inputData) {
        this.fruitVendors = Arrays.stream(inputData.vendors)
                .map(vendor -> new FruitVendor(
                        vendor.id,
                        Arrays.stream(inputData.fruitBaskets)
                                .filter(fb -> fb.vendorId.equals(vendor.id))
                                .map(fb -> new FruitBasket(fb.id, fb.costInOre, fb.basketType))
                                .toList()
                ))
                .collect(Collectors.toMap(v -> v.id, v -> v));
    }

    @Override
    public void persist(FruitVendor fruitVendor) {
        this.fruitVendors.put(fruitVendor.id, fruitVendor);
    }

    @Override
    public Collection<FruitVendor> getVendorsOnRoute() {
        return this.fruitVendors.values();
    }
}
