package com.wayatask.Fruits.framework.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wayatask.Fruits.domain.model.Fruit;

public class InputData {

    public static class Vendor {
        @JsonProperty("id") public Integer id;
        @JsonProperty("name") public String name;
    }

    public static class FruitBasket {
        @JsonProperty("id") public Integer id;
        @JsonProperty("vendor-id") public Integer vendorId;
        @JsonProperty("cost-in-ore") public Integer costInOre;
        @JsonProperty("basket-type") public Fruit basketType;
    }

    @JsonProperty("vendors") public Vendor[] vendors;
    @JsonProperty("fruit-baskets") public FruitBasket[] fruitBaskets;
}
