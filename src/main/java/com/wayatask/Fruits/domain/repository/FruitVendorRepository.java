package com.wayatask.Fruits.domain.repository;

import com.wayatask.Fruits.domain.model.FruitVendor;

import java.util.Collection;

public interface FruitVendorRepository {

    void persist(FruitVendor fruitVendor);

    Collection<FruitVendor> getVendorsOnRoute();
}
