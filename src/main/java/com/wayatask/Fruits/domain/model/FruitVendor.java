package com.wayatask.Fruits.domain.model;

import java.util.*;

public class FruitVendor {

    final public Integer id;
    final private Collection<FruitBasket> fruitBaskets;

    public FruitVendor(
           Integer id,
           Collection<FruitBasket> fruitBaskets
    ) {
        this.id = id;
        this.fruitBaskets = fruitBaskets;
    }

    /**
     * @return The lowest possible cost for the combination of fruits provided.
     *         If not all fruits are available, no cost is returned
     */
    public Optional<Integer> getLowestCostForFruits(Set<Fruit> fruitSet) {
        int[] s = fruitSet
                .stream()
                .map(fruit -> this.fruitBaskets
                        .stream()
                        .filter(basket -> basket.basketType.equals(fruit))
                        .min(Comparator.comparingInt(basket -> basket.costInOre))
                )
                .filter(Optional::isPresent)
                .map(Optional::get)
                .mapToInt(basket -> basket.costInOre)
                .toArray();

        return s.length == fruitSet.size() && !fruitSet.isEmpty()
                ? Optional.of(Arrays.stream(s).sum()) : Optional.empty();
    }
}
