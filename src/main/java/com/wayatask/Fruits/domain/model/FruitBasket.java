package com.wayatask.Fruits.domain.model;

public class FruitBasket {

    public final Integer id;
    public final Integer costInOre;
    public final Fruit basketType;

    public FruitBasket(
            Integer id,
            Integer costInOre,
            Fruit basketType
    ) {
        this.id = id;
        this.costInOre = costInOre;
        this.basketType = basketType;
    }
}
