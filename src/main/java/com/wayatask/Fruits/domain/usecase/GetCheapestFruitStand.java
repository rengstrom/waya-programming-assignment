package com.wayatask.Fruits.domain.usecase;

import com.wayatask.Fruits.domain.model.Fruit;
import com.wayatask.Fruits.domain.model.FruitVendor;
import com.wayatask.Fruits.domain.repository.FruitVendorRepository;
import org.javatuples.Pair;

import java.util.Comparator;
import java.util.Optional;
import java.util.Set;

public class GetCheapestFruitStand {

    private final FruitVendorRepository fruitVendorRepository;

    public GetCheapestFruitStand(FruitVendorRepository fruitVendorRepository) {
        this.fruitVendorRepository = fruitVendorRepository;
    }

    /**
     * @return a FruitVendor and its cheapest deal for requested fruits.
     * If no vendor exists that have all requested fruits no vendor is returned.
     */
    public Optional<Pair<FruitVendor, Integer>> withFruits(Set<Fruit> fruitSet) {
        return this.fruitVendorRepository.getVendorsOnRoute()
                .stream()
                .map(v -> new Pair<FruitVendor, Optional<Integer>>(v, v.getLowestCostForFruits(fruitSet)))
                .filter(p -> p.getValue1().isPresent())
                .map(v -> new Pair<FruitVendor, Integer>(v.getValue0(), v.getValue1().get()))
                .min(Comparator.comparingInt(Pair::getValue1));
    }
}
