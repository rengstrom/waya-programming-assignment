package com.wayatask.Fruits;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Optional;
import java.util.concurrent.Callable;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wayatask.Fruits.domain.model.Fruit;
import com.wayatask.Fruits.domain.model.FruitVendor;
import com.wayatask.Fruits.domain.repository.FruitVendorRepository;
import com.wayatask.Fruits.domain.usecase.GetCheapestFruitStand;
import com.wayatask.Fruits.framework.model.InputData;
import com.wayatask.Fruits.framework.repository.FruitVendorInMemoryRepository;
import org.javatuples.Pair;
import org.springframework.stereotype.Component;
import picocli.CommandLine;
import picocli.CommandLine.Option;

@Component
@CommandLine.Command(name = "fruits", mixinStandardHelpOptions = true, version = "fruits-1.0", description = "Fruit stand finder")
public class Command implements Callable<Integer> {

    @Option(names = {"-p",
            "--path"}, required = true, description = "Path to where vendor json data is stored")
    String path;

    @Option(names = "--fruits",
            description = "Requested fruits separated by ,. Possible values: ${COMPLETION-CANDIDATES}", split = ",",
            required = true
    )
    HashSet<Fruit> fruits;

    @Override
    public Integer call() throws Exception {

        InputData inputData;

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            inputData = objectMapper.readValue(new File(this.path), InputData.class);
        } catch (JsonMappingException e) {
            System.out.println("Failed to parse data, exiting");
            return 1;
        } catch (FileNotFoundException e) {
            System.out.println("Could not open " + path);
            return 1;
        }

        FruitVendorRepository fruitVendorRepository = new FruitVendorInMemoryRepository(inputData);
        GetCheapestFruitStand getCheapestFruitStand = new GetCheapestFruitStand(fruitVendorRepository);
        Optional<Pair<FruitVendor, Integer>> result = getCheapestFruitStand.withFruits(fruits);

        if (result.isPresent()) {
            FruitVendor vendor = result.get().getValue0();
            Integer cost = result.get().getValue1();
            System.out.println("Cheapest vendor is " + vendor.id + " with a cost of " + cost);
        } else {
            System.out.println("No vendor sells all of the requested fruits");
        }

        return 0;
    }
}