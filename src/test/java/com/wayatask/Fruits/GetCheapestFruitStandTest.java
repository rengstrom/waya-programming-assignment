package com.wayatask.Fruits;

import com.wayatask.Fruits.domain.model.Fruit;
import com.wayatask.Fruits.domain.model.FruitBasket;
import com.wayatask.Fruits.domain.model.FruitVendor;
import com.wayatask.Fruits.domain.repository.FruitVendorRepository;
import com.wayatask.Fruits.domain.usecase.GetCheapestFruitStand;
import org.javatuples.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

@SpringBootTest
class GetCheapestFruitStandTest {

    @Mock
    private FruitVendorRepository fruitVendorRepository;

    private GetCheapestFruitStand getCheapestFruitStand;

    @BeforeEach
    void setUp() {
        this.getCheapestFruitStand = new GetCheapestFruitStand(this.fruitVendorRepository);
    }

    @Test
    public void whenNoVendorHasRequestedFruitThenNoVendorIsReturned() {
        List<FruitVendor> vendors = List.of(
                new FruitVendor(
                        1,
                        List.of(new FruitBasket(1, 10000, Fruit.PEACH))
                )
        );
        Mockito.when(this.fruitVendorRepository.getVendorsOnRoute()).thenReturn(vendors);

        Optional<Pair<FruitVendor, Integer>> result = getCheapestFruitStand.withFruits(
                new HashSet<>(List.of(Fruit.CHERRY))
        );

        Assertions.assertFalse(result.isPresent());
    }

    @Test
    public void whenNoFruitsAreRequestedThenNoVendorIsReturned() {
        List<FruitVendor> vendors = List.of(
                new FruitVendor(
                        1,
                        List.of(
                                new FruitBasket(1, 10000, Fruit.PEACH),
                                new FruitBasket(2, 15000, Fruit.CHERRY)
                        )
                ),
                new FruitVendor(
                        2,
                        List.of(
                                new FruitBasket(3, 9500, Fruit.PEACH),
                                new FruitBasket(4, 12000, Fruit.CHERRY)
                        )
                )
        );
        Mockito.when(this.fruitVendorRepository.getVendorsOnRoute()).thenReturn(vendors);

        Optional<Pair<FruitVendor, Integer>> result = getCheapestFruitStand.withFruits(new HashSet<>());

        Assertions.assertFalse(result.isPresent());
    }

    @Test
    public void whenMultipleVendorsHaveAllRequestedFruitThenTheCheapestAreReturned() {
        List<FruitVendor> vendors = List.of(
                new FruitVendor(
                        1,
                        List.of(
                                new FruitBasket(1, 10000, Fruit.PEACH),
                                new FruitBasket(2, 15000, Fruit.CHERRY)
                        )
                ),
                new FruitVendor(
                        2,
                        List.of(
                                new FruitBasket(3, 9500, Fruit.PEACH),
                                new FruitBasket(4, 12000, Fruit.CHERRY)
                        )
                )
        );
        Mockito.when(this.fruitVendorRepository.getVendorsOnRoute()).thenReturn(vendors);

        Optional<Pair<FruitVendor, Integer>> result = getCheapestFruitStand.withFruits(
                new HashSet<>(Arrays.asList(Fruit.CHERRY, Fruit.PEACH))
        );

        Assertions.assertTrue(result.isPresent());
        Assertions.assertSame(vendors.get(1), result.get().getValue0());
        Assertions.assertEquals(21500, result.get().getValue1());
    }

    @Test
    public void whenVendorHasMultipleFruitBasketsWithDifferentPricesTheCheapestIsReturned() {
        List<FruitVendor> vendors = List.of(
                new FruitVendor(
                        1,
                        List.of(
                                new FruitBasket(1, 10000, Fruit.PEACH),
                                new FruitBasket(2, 1000, Fruit.CHERRY),
                                new FruitBasket(3, 100, Fruit.PEACH),
                                new FruitBasket(4, 10, Fruit.CHERRY),
                                new FruitBasket(5, 1, Fruit.PEACH)
                        )
                )
        );
        Mockito.when(this.fruitVendorRepository.getVendorsOnRoute()).thenReturn(vendors);

        Optional<Pair<FruitVendor, Integer>> result = getCheapestFruitStand.withFruits(
                new HashSet<>(Arrays.asList(Fruit.CHERRY, Fruit.PEACH))
        );

        Assertions.assertTrue(result.isPresent());
        Assertions.assertEquals(11, result.get().getValue1());
    }
}
