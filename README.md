# Waya programming assignment

This assignment is split into multiple extension using different branches.
Below are links to each branch:

* [Base assignment (master)](../../tree/master)
* [Extension 1 (extension-1)](../../tree/extension-1)
* [Extension 2 (extension-2)](../../tree/extension-2)
* [Extension 3 (extension-3)](../../tree/extension-3)
* [Extension 4 (extension-4)](../../tree/extension-4)

### Requirements
* [Docker](https://docs.docker.com/get-docker/)

### Building
Run command to build
```bash
docker run -it --rm -v $(pwd):/src --workdir=/src maven:3.8.3-openjdk-17 mvn package
```

### Testing
Run command to test
```bash
docker run -it --rm -v $(pwd):/src --workdir=/src maven:3.8.3-openjdk-17 mvn test
```

### Usage
For the base assignment usecase, run the command below:
```bash
docker run -it --rm -v $(pwd):/src --workdir=/src maven:3.8.3-openjdk-17 \
    java -jar target/Fruits-1.0.0.jar --path="example.json" --fruits=CHERRY,PEACH
```